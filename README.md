# README #
This custom module allows users to easily integrate slack into their Kentico installation. It currently supports messaging on page workflow, event logs, bizform submissions, staging tasks, and e-commerce orders.

### Compatibility ###
Kentico Version: 12.0.29 (MVC and Portal Engine)

### Supported Events: ###

* Page Production Workflow - Messages can be sent on Check out, Check in, or Undo Checkout. The name of the document and a link to that page will be provided in the message
* Event Log - Messages can be sent whenever an event is posted on the event log of the Kentico application: Additionally there is a basic list of Event Codes to select what event log entries should be sent. 
* Bizform Submissions - Messages can be sent whenever a Kentico Bizform is submitted with a name of the form that was submitted.
* Staging Synchronization - Messages can be sent whenever a staging event is completed
* Ecommerce Order - A Message can be sent with basic info on new E-Commerce orders completed on the Kentico Site. 


### Installation ###

1. Open your projects web application in Visual Studio
2. Install the Nuget Package with Install-Package WakeflySlackIntegration -Version 1.0.0
3. Build the project
4. Open your project in a web browser

### Configuring the Module ###

Before the module can be configured you will need to generate a Slack Webhook. Instructions for generating a Slack Webhook can be found [here](https://api.slack.com/messaging/webhooks#create_a_webhook). 

1. Login to the admin backend of your kentico site
2. Open the Wakefly Slack Integration application under Custom
3. Click New Slack Integration
4. Copy and Paste your Slack Webhook into the text box
5. Check off all the boxes for events you want to be notified about
6. Click Save

### Support, Contributions & Bug Fixes ###

Pull Request can be submitted and will be reviewed/approved for community addtions and bug fixes.

Tickets can be submitted to us via Bitucket (Settings -> Issue Tracker).

We will do our best to respond to issues and pull request but do not commit to any timeframe. 
