using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using WakeflySlackIntegration;

[assembly: RegisterObjectType(typeof(SlackIntegrationInfo), SlackIntegrationInfo.OBJECT_TYPE)]


namespace WakeflySlackIntegration
{
    /// <summary>
    /// Data container class for <see cref="SlackIntegrationInfo"/>.
    /// </summary>
    [Serializable]
    public partial class SlackIntegrationInfo : AbstractInfo<SlackIntegrationInfo>
    {
        /// <summary>
        /// Object type.
        /// </summary>
        public const string OBJECT_TYPE = "wakeflyslackintegration.slackintegration";


        /// <summary>
        /// Type information.
        /// </summary>

        public static readonly ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(SlackIntegrationInfoProvider), OBJECT_TYPE, "WakeflySlackIntegration.SlackIntegration", "SlackIntegrationID", "SlackIntegrationLastModified", "SlackIntegrationGuid", null, null, null, null, null, null)
        {
            ModuleName = "WakeflySlackIntegration",
            TouchCacheDependencies = true,
            LogEvents = true,
            SupportsVersioning = true,
            AllowRestore = true,
            SupportsSearch = false,
        };


        /// <summary>
        /// Slack integration ID.
        /// </summary>
        [DatabaseField]
        public virtual int SlackIntegrationID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SlackIntegrationID"), 0);
            }
            set
            {
                SetValue("SlackIntegrationID", value);
            }
        }


        /// <summary>
        /// Slack webhook.
        /// </summary>
        [DatabaseField]
        public virtual string SlackWebhook
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SlackWebhook"), String.Empty);
            }
            set
            {
                SetValue("SlackWebhook", value);
            }
        }


        /// <summary>
        /// Checkin.
        /// </summary>
        [DatabaseField]
        public virtual bool Checkin
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("Checkin"), false);
            }
            set
            {
                SetValue("Checkin", value);
            }
        }


        /// <summary>
        /// Checkout.
        /// </summary>
        [DatabaseField]
        public virtual bool Checkout
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("Checkout"), false);
            }
            set
            {
                SetValue("Checkout", value);
            }
        }


        /// <summary>
        /// Checkout undo.
        /// </summary>
        [DatabaseField]
        public virtual bool CheckoutUndo
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("CheckoutUndo"), false);
            }
            set
            {
                SetValue("CheckoutUndo", value);
            }
        }


        /// <summary>
        /// Event log.
        /// </summary>
        [DatabaseField]
        public virtual bool EventLog
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("EventLog"), false);
            }
            set
            {
                SetValue("EventLog", value);
            }
        }


        /// <summary>
        /// Event log options.
        /// </summary>
        [DatabaseField]
        public virtual string EventLogOptions
        {
            get
            {
                return ValidationHelper.GetString(GetValue("EventLogOptions"), String.Empty);
            }
            set
            {
                SetValue("EventLogOptions", value, String.Empty);
            }
        }


        /// <summary>
        /// Form fill.
        /// </summary>
        [DatabaseField]
        public virtual bool FormFill
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("FormFill"), false);
            }
            set
            {
                SetValue("FormFill", value);
            }
        }


        /// <summary>
        /// Staging sync.
        /// </summary>
        [DatabaseField]
        public virtual bool StagingSync
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("StagingSync"), false);
            }
            set
            {
                SetValue("StagingSync", value);
            }
        }


        /// <summary>
        /// Ecommerce order.
        /// </summary>
        [DatabaseField]
        public virtual bool EcommerceOrder
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("EcommerceOrder"), false);
            }
            set
            {
                SetValue("EcommerceOrder", value);
            }
        }


        /// <summary>
        /// Slack integration guid.
        /// </summary>
        [DatabaseField]
        public virtual Guid SlackIntegrationGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("SlackIntegrationGuid"), Guid.Empty);
            }
            set
            {
                SetValue("SlackIntegrationGuid", value);
            }
        }


        /// <summary>
        /// Slack integration last modified.
        /// </summary>
        [DatabaseField]
        public virtual DateTime SlackIntegrationLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SlackIntegrationLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SlackIntegrationLastModified", value);
            }
        }


        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            SlackIntegrationInfoProvider.DeleteSlackIntegrationInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            SlackIntegrationInfoProvider.SetSlackIntegrationInfo(this);
        }


        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info.</param>
        /// <param name="context">Streaming context.</param>
        protected SlackIntegrationInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Creates an empty instance of the <see cref="SlackIntegrationInfo"/> class.
        /// </summary>
        public SlackIntegrationInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Creates a new instances of the <see cref="SlackIntegrationInfo"/> class from the given <see cref="DataRow"/>.
        /// </summary>
        /// <param name="dr">DataRow with the object data.</param>
        public SlackIntegrationInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }
    }
}