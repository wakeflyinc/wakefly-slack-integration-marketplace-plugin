﻿using System;
using Newtonsoft.Json;
using System.Text;
using CMS.DataEngine;
using CMS.Base;
using CMS.Helpers;
using System.Net;
using System.Collections.Specialized;
using CMS;
using WakeflySlackIntegration;
using System.Linq;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.OnlineForms;
using CMS.Synchronization;
using CMS.Ecommerce;

[assembly: RegisterModule(typeof(WakeflySlackIntegration.WakeflySlackIntegrationModule))]

namespace WakeflySlackIntegration
{
    class WakeflySlackIntegrationModule : Module
    {

        public WakeflySlackIntegrationModule()
           : base("WakeflySlackIntegration")
        {

        }

        protected override void OnInit()
        {

            base.OnInit();

            SlackIntegrationInfo.TYPEINFO.Events.Update.After += Reinit;

            if (SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault() != null)
            {

                //Settings

                bool CheckinEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().Checkin;
                bool CheckoutEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().Checkout;
                bool UndoEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().CheckoutUndo;
                bool EventLogEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().EventLog;
                bool FormFillEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().FormFill;
                bool StagingEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().StagingSync;
                bool EcommerceEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().EcommerceOrder;

                if (CheckoutEnable)
                {
                    WorkflowEvents.CheckOut.After += CheckoutMessage;
                }
                if (CheckinEnable)
                {
                    WorkflowEvents.CheckIn.After += CheckInMessage;
                }
                if (UndoEnable)
                {
                    WorkflowEvents.UndoCheckOut.After += UndoMessage;
                }
                if (EventLogEnable)
                {
                    EventLogEvents.LogEvent.After += EventLogMessage;
                }
                if (FormFillEnable)
                {
                    BizFormItemEvents.Insert.After += FormFillMessage;
                }
                if (StagingEnable)
                {
                    StagingEvents.Synchronize.After += StagingSyncMessage;
                }
                if (EcommerceEnable)
                {
                    EcommerceEvents.NewOrderCreated.Execute += NewOrderMessage;
                }
            }





        }

        //Reloads the Module settings when the module object is updated in the UI
        private void Reinit(object sender, ObjectEventArgs e)
        {
            bool CheckinEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().Checkin;
            bool CheckoutEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().Checkout;
            bool UndoEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().CheckoutUndo;
            bool EventLogEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().EventLog;
            bool FormFillEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().FormFill;
            bool StagingEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().StagingSync;
            bool EcommerceEnable = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().EcommerceOrder;

            WorkflowEvents.CheckOut.After -= CheckoutMessage;
            WorkflowEvents.CheckIn.After -= CheckInMessage;
            WorkflowEvents.UndoCheckOut.After -= UndoMessage;
            EventLogEvents.LogEvent.After -= EventLogMessage;
            BizFormItemEvents.Insert.After -= FormFillMessage;
            StagingEvents.Synchronize.After -= StagingSyncMessage;


            if (CheckoutEnable)
            {
                WorkflowEvents.CheckOut.After += CheckoutMessage;
            }

            if (CheckinEnable)
            {
                WorkflowEvents.CheckIn.After += CheckInMessage;
            }

            if (UndoEnable)
            {
                WorkflowEvents.UndoCheckOut.After += UndoMessage;
            }

            if (EventLogEnable)
            {
                EventLogEvents.LogEvent.After += EventLogMessage;
            }

            if (FormFillEnable)
            {
                BizFormItemEvents.Insert.After += FormFillMessage;
            }

            if (StagingEnable)
            {
                StagingEvents.Synchronize.After += StagingSyncMessage;
            }

            if (EcommerceEnable)
            {
                EcommerceEvents.NewOrderCreated.Execute += NewOrderMessage;
            }

        }

        private void UndoMessage(object sender, WorkflowEventArgs e)
        {
            PostToSlack("Document <" + URLHelper.GetAbsoluteUrl(e.Document.NodeAliasPath) + "|" + e.Document.DocumentName + "> checked out undone.");
        }

        private void CheckInMessage(object sender, WorkflowEventArgs e)
        {
            PostToSlack("Document <" + URLHelper.GetAbsoluteUrl(e.Document.NodeAliasPath) + "|" + e.Document.DocumentName + "> checked in.");
        }

        private void CheckoutMessage(object sender, WorkflowEventArgs e)
        {
            PostToSlack("Document <" + URLHelper.GetAbsoluteUrl(e.Document.NodeAliasPath) + "|" + e.Document.DocumentName + "> checked out.");
        }
        private void EventLogMessage(object sender, LogEventArgs e)
        {
            string EnabledEventTypes = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().EventLogOptions;
            string[] EnabledTypesList = EnabledEventTypes.Split('|');
            if (Array.Exists(EnabledTypesList, element => element == e.Event.EventCode))
            {
                PostToSlack(e.Event.EventCode + ": " + e.Event.EventDescription);
            }
        }
        private void FormFillMessage(object sender, BizFormItemEventArgs e)
        {
            PostToSlack("New form submission for " + e.Item.BizFormInfo.FormName + ".");
        }
        private void StagingSyncMessage(object sender, CMSEventArgs e)
        {
            PostToSlack("A Kentico Staging operation has been completed");
        }
        private void NewOrderMessage(object sender, NewOrderCreatedEventArgs e)
        {
            PostToSlack("New Ecommerce Order: #" + e.NewOrder.OrderInvoiceNumber + " for " + e.NewOrder.OrderGrandTotal + ".");
        }


        private void PostToSlack(string strMessage)
        {
            string slackWebhook = SlackIntegrationInfoProvider.GetSlackIntegrations().FirstOrDefault().SlackWebhook;
            try
            {
                Message payload = new Message
                {

                    Text = strMessage

                };
                SlackClient client = new SlackClient(slackWebhook);
                client.SendMessage(payload);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("SlackIntegrationModuleLoader", "EXCEPTION", ex);
            }
        }

        public class SlackClient
        {
            private readonly Uri Webhook;
            private readonly Encoding _encoding = new UTF8Encoding();

            public SlackClient(string urlWithAccessToken)
            {
                Webhook = new Uri(urlWithAccessToken);
            }

            //Post a message using a Payload object
            public void SendMessage(Message payload)
            {
                string payloadJson = JsonConvert.SerializeObject(payload);

                using (WebClient client = new WebClient())
                {
                    NameValueCollection data = new NameValueCollection();
                    data["payload"] = payloadJson;

                    var response = client.UploadValues(Webhook, "POST", data);

                    //The response text is usually "ok"
                    string responseText = _encoding.GetString(response);
                }
            }



        }
        public class Message
        {
            [JsonProperty("username")]
            public string Username { get; set; }

            [JsonProperty("channel")]
            public string Channel { get; set; }

            [JsonProperty("text")]
            public string Text { get; set; }
        }
    }
}
