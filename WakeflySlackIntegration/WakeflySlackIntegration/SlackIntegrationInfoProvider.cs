using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace WakeflySlackIntegration
{
    /// <summary>
    /// Class providing <see cref="SlackIntegrationInfo"/> management.
    /// </summary>
    public partial class SlackIntegrationInfoProvider : AbstractInfoProvider<SlackIntegrationInfo, SlackIntegrationInfoProvider>
    {
        /// <summary>
        /// Creates an instance of <see cref="SlackIntegrationInfoProvider"/>.
        /// </summary>
        public SlackIntegrationInfoProvider()
            : base(SlackIntegrationInfo.TYPEINFO)
        {
        }


        /// <summary>
        /// Returns a query for all the <see cref="SlackIntegrationInfo"/> objects.
        /// </summary>
        public static ObjectQuery<SlackIntegrationInfo> GetSlackIntegrations()
        {
            return ProviderObject.GetObjectQuery();
        }


        /// <summary>
        /// Returns <see cref="SlackIntegrationInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SlackIntegrationInfo"/> ID.</param>
        public static SlackIntegrationInfo GetSlackIntegrationInfo(int id)
        {
            return ProviderObject.GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified <see cref="SlackIntegrationInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SlackIntegrationInfo"/> to be set.</param>
        public static void SetSlackIntegrationInfo(SlackIntegrationInfo infoObj)
        {
            ProviderObject.SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified <see cref="SlackIntegrationInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SlackIntegrationInfo"/> to be deleted.</param>
        public static void DeleteSlackIntegrationInfo(SlackIntegrationInfo infoObj)
        {
            ProviderObject.DeleteInfo(infoObj);
        }


        /// <summary>
        /// Deletes <see cref="SlackIntegrationInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SlackIntegrationInfo"/> ID.</param>
        public static void DeleteSlackIntegrationInfo(int id)
        {
            SlackIntegrationInfo infoObj = GetSlackIntegrationInfo(id);
            DeleteSlackIntegrationInfo(infoObj);
        }
    }
}